module.exports = function(api) {
  api.cache(true);
  return {
    presets: ['babel-preset-expo'],
    plugins: [
      [
        'module-resolver',
        {
          root: ['./'],
          extensions: [
            '.ios.ts',
            '.android.ts',
            '.ts',
            '.ios.tsx',
            '.android.tsx',
            '.tsx',
            '.jsx',
            '.js',
            '.json',
          ],
          alias: {
            scenes: './scenes',
            components_atoms: './components/atoms',
            components_molecules: './components/molecules',
            components_organisms: './components/organisms',
            helpers: './helpers',
            types: './types',
            dal: './dal',
            styles: './styles'
          },
        },
      ],
    ],
  };
};
