type JournalEntry = {
    id: number,
    mood: string,
    moodIntensity: number,
    dateTime: string
}

type JournalDateGroup = {
    dateTime: string
    entries: Array<JournalEntry>
}

export {JournalEntry, JournalDateGroup};