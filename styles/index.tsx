import * as Colors from 'styles/colors';
import * as Typography from 'styles/typography';

export { Colors, Typography };