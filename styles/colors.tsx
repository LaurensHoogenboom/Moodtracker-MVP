//grey
export const GREY_PRIMARY = '#707070';
export const GREY_SECONDARY = '#B8B8B8';
//background
export const SCREEN_BACKGROUND_DEFAULT = 'rgb(250,250,250)';
export const BACKGROUND_LIGHT = 'rgb(240,240,240)';
export const BACKGROUND_NORMAL = 'rgb(220,220,220)';
export const BACKGROUND_DARK = 'rgb(180,180,180)';
//blue
export const BLUE_LIGHT = '#8BCAF3';
export const BLUE_DARK = 'rgb(77,177,243)';
//green
export const GREEN = 'rgb(130,172,32)';
//orange
export const ORANGE = 'rgb(255,173,51)'
//white
export const WHITE_PRIMARY = '#FFFFFF';
export const WHITE_SECONDARY = 'rgba(255,255,255,0.6)';
//shadow
export const BLACK_SHADOW = 'rgba(0,0,0,0.16)';
