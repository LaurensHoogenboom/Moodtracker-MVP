import AsyncStorage from '@react-native-community/async-storage';
import { JournalEntry } from 'types/journalEntries';

const getEntries = async () => {
    try {
        const jsonValue = await AsyncStorage.getItem('journalEntries');
        return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
        console.log(e)
    }
}

const setEntries = async (entries: Array<JournalEntry>) => {
    try {
        const stringData = JSON.stringify(entries);
        await AsyncStorage.setItem('journalEntries', stringData);
    } catch (e) {
        console.log(e)
    }
}

const deleteEntries = (entriesToRemove:Array<JournalEntry>, callback?: () => void) => {
    entriesToRemove.forEach(entryToRemove => {
        getEntries().then(currentEntries => {
            let entries:Array<JournalEntry> = currentEntries;
    
            let indexOfItemToRemove = entries.map(entry => {
                return entry.id;
            }).indexOf(entryToRemove.id);
    
            entries.splice(indexOfItemToRemove, 1);
    
            if (callback) {
                setEntries(entries).then(() => {
                    callback();
                });
            } else {
                setEntries(entries);
            }            
        });
    });
}

const wipeEntries = async () => {
    try {
        await AsyncStorage.removeItem('journalEntries')
    } catch (e) {
        console.log(e)
    }
}

export { getEntries, setEntries, wipeEntries, deleteEntries };