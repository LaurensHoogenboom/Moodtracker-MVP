//navigation
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
//react
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
//pages
import Map from 'components/organisms/map'
import Moodtracker from 'components/organisms/moodtracker'
import Journal from 'components/organisms/journal'
//icons
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faBook, faRoute, faFeatherAlt } from '@fortawesome/free-solid-svg-icons'
//style
import * as Colors from 'styles/colors';


const Tab = createBottomTabNavigator();

const mainStyle = StyleSheet.create({
    tabbar: {
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: "white",
        position: 'absolute',
        bottom: 0,
        height: 60,
        zIndex: 8,
    },
    moodtrackerIcon: {
        height: 45,
        width: 45,
        borderRadius: 10,
        backgroundColor: Colors.BACKGROUND_NORMAL,
        alignItems: "center",
        justifyContent: "center"
    }
})

const Main = () => {
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <NavigationContainer>
                <Tab.Navigator
                    screenOptions={
                        ({ route }) => ({
                            tabBarIcon: ({ color, focused }) => {
                                switch (route.name) {
                                    case "Journal": {
                                        return <FontAwesomeIcon icon={faBook} size={25} color={color} />;
                                    }
                                    case "Moodtracker": {
                                        return (
                                            <View style={[
                                                mainStyle.moodtrackerIcon,
                                                {
                                                    backgroundColor: focused ? Colors.BLUE_DARK : Colors.BACKGROUND_NORMAL
                                                }
                                            ]}
                                            >
                                                <FontAwesomeIcon
                                                    icon={faFeatherAlt}
                                                    size={25}
                                                    color={ focused ? Colors.WHITE_PRIMARY : Colors.GREY_PRIMARY}
                                                />
                                            </View>
                                        );
                                    }
                                    case "Map": {
                                        return <FontAwesomeIcon icon={faRoute} size={25} color={color} />;
                                    }
                                }
                            },
                        })
                    }
                    tabBarOptions={{
                        activeTintColor: Colors.BLUE_DARK,
                        inactiveTintColor: Colors.GREY_SECONDARY,
                        style: mainStyle.tabbar,
                        showLabel: false
                    }}
                >
                    <Tab.Screen name="Journal" component={Journal} />
                    <Tab.Screen name="Moodtracker" component={Moodtracker} />
                    <Tab.Screen name="Map" component={Map} />
                </Tab.Navigator>
            </NavigationContainer>
        </SafeAreaView>
    );
}

export default Main;