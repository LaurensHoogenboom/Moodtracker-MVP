import { setEntries } from 'dal/entries';

const seedMoods = () => {
    const journalEntries = [
        {
            id: Math.floor(Math.random() * 1000000),
            mood: "Happy",
            moodIntensity: "5",
            dateTime: new Date("2020-10-13")
        },
        {
            id: Math.floor(Math.random() * 1000000),
            mood: "Sad",
            moodIntensity: "5",
            dateTime: new Date("2020-10-13")
        },
        {
            id: Math.floor(Math.random() * 1000000),
            mood: "Relaxed",
            moodIntensity: "3",
            dateTime: new Date("2020-10-12")
        },
        {
            id: Math.floor(Math.random() * 1000000),
            mood: "Happy",
            moodIntensity: "6",
            dateTime: new Date("2020-10-12")
        }
    ]

    setEntries(journalEntries).then(() => {
        console.log('Seeded!')
    })    
}

export default seedMoods;