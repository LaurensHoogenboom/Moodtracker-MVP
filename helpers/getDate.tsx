const getDate = {
    dMy: (date: string) => {
        const month = [
            "Januari",
            "Februari",
            "March",
            "April",
            "May",
            "June",
            "Juli",
            "August",
            "September",
            "October",
            "November",
            "December"
        ];

        const jsDate = new Date(date);
        return `${jsDate.getDate()} ${month[jsDate.getMonth()]} ${jsDate.getFullYear()} `
    },
    time24: (date: string) => {
        const jsDate = new Date (date);
        const hours = ("0" + jsDate.getHours()).slice(-2);
        const minutes = ("0" + jsDate.getMinutes()).slice(-2);

        return `${hours}:${minutes}`
    }
}

export default getDate;