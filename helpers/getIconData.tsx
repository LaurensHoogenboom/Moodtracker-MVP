import { faSmile, faSadCry, faLaugh, faTimes, faTrash, faRoute } from '@fortawesome/free-solid-svg-icons';
import * as Colors from 'styles/colors';

const getIconData = (emotion: string) => {
    switch (emotion) {
        case "Relaxed": {
            return ({
                icon: faSmile,
                color: Colors.BLUE_DARK
            });
        }
        case "Sad": {
            return ({
                icon: faSadCry,
                color: Colors.ORANGE
            });
        }
        case "Happy": {
            return ({
                icon: faLaugh,
                color: Colors.GREEN
            });
        }
        case "Cross": {
            return ({
                icon: faTimes,
                color: Colors.BLUE_DARK
            });
        }
        case "Trash": {
            return ({
                icon: faTrash,
                color: Colors.ORANGE
            })
        }
        case "Route": {
            return ({
                icon: faRoute,
                color: Colors.GREEN
            })
        }
        default: {
            return ({
                icon: faSmile,
                color: Colors.BLUE_DARK
            });
        }
    }
}

export default getIconData;