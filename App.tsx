import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { SafeAreaProvider } from 'react-native-safe-area-context'
import Main from 'scenes/main';
import { useFonts } from '@use-expo/font';
import { AppLoading } from 'expo';

const Tab = createBottomTabNavigator();



const App = () => {
  const [fontsLoaded] = useFonts({
    "AlexaStd": require("./assets/fonts/AlexaStd.otf"),
  });

  if (!fontsLoaded) {
    return <AppLoading />
  } else {
    return (
      <SafeAreaProvider>
        <Main />
      </SafeAreaProvider>
    );
  }
}

export default App;





