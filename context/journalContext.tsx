import JournalEntryList from 'components/molecules/journal/journalEntryList';
import { createContext } from 'react';
import { JournalEntry } from 'types/journalEntries';


const JournalContext = createContext({
    setShowDetailModal: (isVisible:boolean) => {},
    setEntryToShowDetailFrom: (entry: JournalEntry) => {},
    updateEntryList: () => {}
})

export default JournalContext;

