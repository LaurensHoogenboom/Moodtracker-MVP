//react
import React, { useState, useReducer } from 'react';
//styles
import * as Colors from 'styles/colors';
import * as Typography from 'styles/typography';
//components
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import { ProgressCircle } from 'components/atoms/progress';
import Mood from 'components/atoms/mood';
//helpers
import useInterval from "@rooks/use-interval";
import getIconData from 'helpers/getIconData';

const style = StyleSheet.create({
    container: {
        alignItems: "center",
        flex: 1,
        justifyContent: "center"
    },
    moodSelection: {
        height: 140,
        width: 140,
        backgroundColor: Colors.BACKGROUND_NORMAL,
        borderRadius: 85,
        position: "relative",
        alignItems: 'center',
        justifyContent: 'center',
    },
    header: {
        textAlign: "center",
        marginBottom: 60,
        fontSize: Typography.FONT_SIZE_CAPTIONS,
        color: Colors.GREY_PRIMARY,
        fontFamily: "AlexaStd"
    },
    iconScrollWrapper: {
        height: 110,
        width: 110,
        position: "absolute",
        zIndex: 3,
        borderRadius: 50,
        overflow: "hidden"
    },
    iconScroll: {
        height: 110,
        width: 110
    },
    moodIndicator: {
        flexDirection: "row",
        backgroundColor: Colors.BACKGROUND_NORMAL,
        marginTop: 40,
        borderRadius: 20
    },
    moodDescription: {
        textAlign: "center",
        fontSize: Typography.FONT_SIZE_DEFAULT,
        color: Colors.GREY_PRIMARY,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
    },
    moodIntensity: {
        height: 40,
        width: 40,
        backgroundColor: Colors.BACKGROUND_DARK,
        borderRadius: 20,
        textAlign: "center",
        textAlignVertical: "center",
        color: Colors.GREY_PRIMARY
    }
})

type Moodform = {
    callback: any
}

function reducer(state: any, action: any) {
    switch (action.type) {
        case "increment":
            if (state >= 7) {
                return 0
            } else {
                return state + 1;
            }
        case "reset":
            return 0
        default:
            return state;
    }
}

const Moodform = (props: Moodform) => {
    const [mood, setMood] = useState('Relaxed');
    const [moodIntensity, setIntensity] = useReducer(reducer, 0);
    const [moodDescription, setMoodDescription] = useState("Relaxed");
    const [moodColor, setMoodColor] = useState(Colors.BLUE_DARK);

    const allMoods = [
        "Relaxed",
        "Sad",
        "Happy"
    ];

    const { start, stop } = useInterval(() => {
        setIntensity({ type: "increment" });
    }, 500, false);

    const startTimer = () => {
        start()
    }

    const stopTimer = () => {
        stop();

        if (moodIntensity > 0) {
            props.callback({
                id: Math.floor(Math.random() * 1000000),
                mood: mood,
                moodIntensity: moodIntensity,
                dateTime: new Date
            });
        }

        setIntensity({ type: "reset" });
    }

    const handleScroll = (event: Object) => {
        let index = Math.round(event.nativeEvent.contentOffset.x / 110);
        let emojiData = getIconData(allMoods[index]);

        setMood(allMoods[index]);
        setMoodDescription(allMoods[index]);
        setMoodColor(emojiData.color);
    }

    const moods = allMoods.map((mood, i) => {
        return (
            <Mood
                title={mood}
                onPressIn={startTimer}
                onPressOut={stopTimer}
                key={mood}
            />
        );
    });

    return (
        <View style={style.container}>
            <Text style={style.header}>How are you feeling? </Text>

            <View style={style.moodSelection}>
                <View style={style.iconScrollWrapper}>
                    <ScrollView
                        style={style.iconScroll}
                        pagingEnabled={true}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        onMomentumScrollEnd={handleScroll}
                        keyboardShouldPersistTaps="always"
                        decelerationRate="fast"
                    >
                        {moods}
                    </ScrollView>
                </View>

                <ProgressCircle
                    size={140}
                    strokeWidth={10}
                    stepsFilled={moodIntensity}
                    steps={7}
                    foregroundColor={moodColor}
                />
            </View>

            <View style={style.moodIndicator}>
                <Text style={style.moodIntensity}>
                    {moodIntensity}
                </Text>

                <Text style={style.moodDescription}>{moodDescription}</Text>
            </View>
        </View>
    );
}

export default Moodform;