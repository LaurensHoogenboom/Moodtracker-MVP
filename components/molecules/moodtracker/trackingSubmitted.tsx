//react
import React from 'react';
//style
import * as Typography from 'styles/typography';
import * as Colors from 'styles/colors'
//components
import { StyleSheet, View, Text } from 'react-native';
import { Button } from 'components/atoms/formElements';
//icons
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';

const style = StyleSheet.create({
    container: {
        alignItems: "center",
        paddingBottom: 60,
        flex: 1
    },
    notification: {
        alignItems: "center",
        justifyContent: "center",
        flex: 1
    },
    icon: {
        marginBottom: 40
    },
    text: {
        fontSize: Typography.FONT_SIZE_CAPTIONS,
        color: Colors.GREY_PRIMARY,
        fontFamily: 'AlexaStd'
    },
    journalButton: {
        marginBottom: 20
    }
});

type TrackingSubmitted = {
    resetForm: any,
    recommendedAction: any
};

const TrackingSubmitted = (props: TrackingSubmitted) => {
    return (
        <View style={style.container}>
            <View style={style.notification}>
                <FontAwesomeIcon
                    icon={faCheckCircle}
                    size={70}
                    color={Colors.GREEN}
                    style={style.icon}
                />

                <Text style={style.text}>Entry written in journal. </Text>
            </View>

            <View>
                <Button
                    title="View in your Journal"
                    onPress={() => {
                        props.resetForm();
                        props.recommendedAction();
                    }}
                    containerStyle={style.journalButton}
                />
                <Button
                    title="Back"
                    onPress={() => {
                        props.resetForm();
                    }}
                    noBackground={true}
                />
            </View>
        </View>
    )
}

export default TrackingSubmitted;