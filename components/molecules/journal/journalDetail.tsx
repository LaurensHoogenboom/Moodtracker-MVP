//react
import React, { useContext } from 'react';
//components
import { Text, View, StyleSheet, Modal } from 'react-native';
import IconWithDescription from 'components/atoms/IconWithDescription';
import JournalDetailContent from 'components/atoms/journal/journalDetailContent';
//style
import * as Colors from 'styles/colors';
import * as Typography from 'styles/typography';
//types
import { JournalEntry } from 'types/journalEntries';
//helpers
import getDate from 'helpers/getDate';
//dal
import { deleteEntries } from 'dal/entries';
//context
import JournalContext from 'context/journalContext';

const style = StyleSheet.create({
    modalView: {
        backgroundColor: Colors.WHITE_PRIMARY,
        width: "100%",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        marginTop: 30,
        alignItems: "center",
        elevation: 5,
        flex: 1
    },
    title: {
        padding: 20,
        alignItems: "center"
    },
    actions: {
        paddingTop: 10,
        paddingBottom: 10,
        flexDirection: "row"
    },
    actionButton: {
        width: "33.33333%"
    },
    dateText: {
        fontFamily: 'AlexaStd',
        fontSize: Typography.FONT_SIZE_CAPTIONS,
        color: Colors.GREY_PRIMARY,
        paddingBottom: 10
    },
    timeText: {
        fontSize: Typography.FONT_SIZE_DEFAULT,
        color: Colors.GREY_SECONDARY
    }
})

type JournalDetail = {
    entry: JournalEntry,
    isVisible: boolean,
}

const JournalDetail = (props: JournalDetail) => {
    const journal = useContext(JournalContext);

    if (props.entry) {
        const deleteCurrentEntry = () => {
            let entries: Array<JournalEntry> = [];
            entries.push(props.entry);

            const closeDetailUpdateJournal = () => {
                journal.updateEntryList();
                journal.setShowDetailModal(false);
            }

            deleteEntries(entries, closeDetailUpdateJournal);
        }

        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={props.isVisible}
            >
                <View style={style.modalView}>
                    <View style={style.title}>
                        <Text style={style.dateText}>
                            {getDate.dMy(props.entry?.dateTime)}
                        </Text>
                        <Text style={style.timeText}>
                            {getDate.time24(props.entry?.dateTime)}
                        </Text>
                    </View>

                    <JournalDetailContent entry={props.entry}/>

                    <View style={style.actions}>
                        <IconWithDescription
                            title="Delete"
                            icon="Trash"
                            callback={() => {
                                deleteCurrentEntry()
                            }}
                            style={style.actionButton}
                        />
                        <IconWithDescription
                            title="Close"
                            icon="Cross"
                            callback={() => { journal.setShowDetailModal(false) }}
                            style={style.actionButton}
                        />
                        <IconWithDescription
                            title="Show in route"
                            icon="Route"
                            callback={() => { journal.setShowDetailModal(false) }}
                            style={style.actionButton}
                        />
                    </View>
                </View>
            </Modal>
        )
    } else {
        return (
            <Text>No entry selected</Text>
        );
    }
}

export default JournalDetail;