//react
import React, { useState } from 'react';
//components
import { View, ScrollView, StyleSheet } from 'react-native';
import JournalEntryListGroup from 'components/atoms/journal/journalEntryListGroup';
import JournalEmptyList from 'components/atoms/journal/journalEmtpyList';
//types
import { JournalDateGroup, JournalEntry } from 'types/journalEntries';
//styles
import * as Colors from 'styles/colors';
import * as Typography from 'styles/typography';
import getDate from 'helpers/getDate';

const style = StyleSheet.create({
    JournalEntryListWrapper: {
        flex: 1
    },
    JournalEntryList: {
        flexGrow: 1,
        paddingBottom: 60
    },
    GroupList: {
        borderLeftWidth: 2,
        borderLeftColor: Colors.GREY_SECONDARY,
        paddingLeft: 20
    },
    GroupListWrapper: {
        width: "100%"
    },
    ListSectionDate: {
        fontSize: Typography.FONT_SIZE_DEFAULT,
        paddingBottom: 20
    }
})

type JournalEntryList = {
    journalDateGroups: Array<JournalDateGroup>,
    emptyListNavigationCallback: () => void,
}

const JournalEntryList = (props: JournalEntryList) => {
    if (!props.journalDateGroups || props.journalDateGroups.length === 0) {
        return (
            <JournalEmptyList emptyListNavigationCallback={props.emptyListNavigationCallback} />
        );
    } else {
        const groups = props.journalDateGroups.map((journalGroup, i) => {
            const currentDateFormatted = getDate.dMy((new Date).toString());
            const journalDateFormatted = getDate.dMy(journalGroup.dateTime);

            return (
                <JournalEntryListGroup
                    journalGroup={journalGroup}
                    key={journalGroup.dateTime}
                    isCurrentDate={journalDateFormatted === currentDateFormatted ? true : false}
                />
            );
        });

        return (
            <View style={style.JournalEntryListWrapper}>
                <ScrollView contentContainerStyle={style.JournalEntryList}>
                    <View style={{ flex: 1, flexDirection: 'column', flexWrap: 'wrap' }}>
                        {groups}
                    </View>
                </ScrollView>
            </View>
        );
    }
}


export default JournalEntryList;