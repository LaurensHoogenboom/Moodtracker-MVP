//react
import React from 'react';
//components
import { View, StyleSheet, useWindowDimensions } from 'react-native';
//styles
import * as Colors from 'styles/colors';

const style = StyleSheet.create({
    journalRibbon: {
        width: 30,
        paddingTop: 30,
        paddingBottom: 30
    },
    circle: {
        height: 20,
        width: 20,
        backgroundColor: Colors.BACKGROUND_NORMAL,
        borderRadius: 10,
        borderWidth: 3,
        marginLeft: 10,
        borderColor: Colors.BACKGROUND_LIGHT,
        marginBottom: 40,
        position: "relative",
        justifyContent: "center"
    },
    bar: {
        width: 30,
        height: 5,
        right: 5,
        borderRadius: 4,
        backgroundColor: Colors.BACKGROUND_DARK,
        position: "absolute",
    }
})

const JournalRibbonPart = () => {
    return (
        <View style={style.circle}>
            <View style={style.bar}>
            </View>
        </View>
    );
}

const JournalRibbon = () => {
    const window = useWindowDimensions();
    const windowHeight = window.height;

    const numberOfRibbonParts = Math.round(windowHeight - 120 / 40 );

    let parts: Array<JSX.Element> = [];
    for (let i = 1; i <= numberOfRibbonParts; i++) {
        parts.push(<JournalRibbonPart key={i} />);
    }

    return (
        <View style={style.journalRibbon}>
            {parts}
        </View>
    );
}

export default JournalRibbon;