import React from 'react';
import {TouchableWithoutFeedback, StyleSheet} from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import getIconData from 'helpers/getIconData';


const style = StyleSheet.create({
    iconWrapper: {
        height: 110,
        width: 110,
        borderRadius: 55
    },
})

type Mood = {
    title: string,
    onPressIn: any,
    onPressOut: any
}

const Mood = (props: Mood) => {
    const emojiData = getIconData(props.title);

    return (
        <TouchableWithoutFeedback
            style={style.iconWrapper}
            onPressIn={props.onPressIn}
            onPressOut={props.onPressOut}
            delayPressIn={0}
        >
            <FontAwesomeIcon
                icon={emojiData.icon}
                size={110}
                color={emojiData.color}
            />
        </TouchableWithoutFeedback>
    )
}

export default Mood;