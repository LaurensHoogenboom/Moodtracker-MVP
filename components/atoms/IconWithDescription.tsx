//react
import React from 'react';
//components
import { Text, View, StyleSheet, TouchableHighlight } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
//style
import * as Colors from 'styles/colors';
import * as Typography from 'styles/typography';
import getIconData from 'helpers/getIconData';

const style = StyleSheet.create({
    wrapper: {
        alignItems: "center"
    },
    icon: {
        marginBottom: 8
    },
    text: {
        fontSize: Typography.FONT_SIZE_DEFAULT,
        color: Colors.GREY_SECONDARY
    }
})

type IconWithDescription = {
    title: string,
    icon: string,
    callback?: () => void,
    style?: object,
    iconColor?: string,
    iconSize?: number,
    textstyle?: object
}

const IconWithDescription = (props: IconWithDescription) => {
    const iconData = getIconData(props.icon);

    return (
        <TouchableHighlight onPress={props.callback ? props.callback : undefined} style={props.style}>
            <View style={style.wrapper}>
                <FontAwesomeIcon
                    style={style.icon}
                    icon={iconData.icon}
                    size={props.iconSize ? props.iconSize : 20}
                    color={props.iconColor ? props.iconColor : iconData.color}
                />

                <Text style={[style.text, props.textstyle]}>
                    {props.title}
                </Text>
            </View>
        </TouchableHighlight>
    );
}


export default IconWithDescription;