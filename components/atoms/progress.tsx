//react
import React from 'react';
//components
import { View, StyleSheet } from 'react-native';
import { Svg, Circle } from 'react-native-svg';
//style
import * as Colors from 'styles/colors';

const calcFillWidth = (steps: number, stepsFilled: number) => {
    return (
        `${Math.round(stepsFilled / steps * 100)}%`
    );
}

const style = StyleSheet.create({
    container: {
        height: 10,
        borderRadius: 5,
        backgroundColor: Colors.BACKGROUND_NORMAL
    },
    fill: {
        height: 10,
        borderRadius: 5
    }
})

type ProgressBar = {
    steps: number,
    stepsFilled: number,
    foregroundColor?: string,
    backgroundColor?: string,
    containerStyle?: object,
    width?: number | string
}

const ProgressBar = (props: ProgressBar) => {
    const fillWidth = calcFillWidth(props.steps, props.stepsFilled);

    return (
        <View
            style={[
                style.container,
                {
                    backgroundColor: props.backgroundColor ? props.backgroundColor : Colors.BACKGROUND_NORMAL,
                    width: props.width ? props.width : "100%"
                },
                props.containerStyle,
            ]}
        >
            <View style={[
                style.fill,
                {
                    backgroundColor: props.foregroundColor ? props.foregroundColor : Colors.BLUE_DARK,
                    width: fillWidth
                }
            ]}>
            </View>
        </View>
    );
}

type ProgressCircle = {
    size: number,
    strokeWidth: number,
    steps: number,
    stepsFilled: number,
    backgroundColor?: string,
    foregroundColor?: string
}

const ProgressCircle = (props: ProgressCircle) => {
    const { size, strokeWidth } = props;
    const radius = (size - strokeWidth) / 2;
    const circumference = radius * 2 * Math.PI;
    const svgProgress = props.steps - props.stepsFilled;

    return (
        <Svg width={size} height={size}>
            {/* Background Circle */}
            <Circle
                stroke={props.backgroundColor ? props.backgroundColor : Colors.BACKGROUND_NORMAL}
                fill="none"
                cx={size / 2}
                cy={size / 2}
                r={radius}
                {...{ strokeWidth }}
            />

            {/* Progress Circle */}
            <Circle
                stroke={props.foregroundColor ? props.foregroundColor : Colors.BLUE_DARK}
                fill="none"
                cx={size / 2}
                cy={size / 2}
                r={radius}
                strokeDasharray={`${circumference} ${circumference}`}
                strokeDashoffset={radius * Math.PI * 2 * (svgProgress / props.steps)}
                strokeLinecap="round"
                transform={`rotate(-90, ${size / 2}, ${size / 2})`}
                {...{ strokeWidth }}
            />
        </Svg>
    );
}

export { ProgressBar, ProgressCircle };