//react
import React from 'react';
//components
import { Text, ScrollView, StyleSheet } from 'react-native';
import { Button } from 'components/atoms/formElements';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faFeatherAlt } from '@fortawesome/free-solid-svg-icons';
//styles
import * as Colors from 'styles/colors';
import * as Typography from 'styles/typography';

const style = StyleSheet.create({
    JournalEntryListEmpty: {
        flexGrow: 1,
        paddingTop: 20,
        paddingBottom: 60,
        justifyContent: "center",
        alignItems: "center"
    },
    icon: {
        marginBottom: 30
    },
    NoEntriesText: {
        color: Colors.GREY_SECONDARY,
        marginBottom: 30,
        textAlign: "center",
        fontSize: Typography.FONT_SIZE_DEFAULT
    },
})

type JournalEmptyList = {
    emptyListNavigationCallback: any
}

const JournalEmptyList = (props: JournalEmptyList) => {
    return (
        <ScrollView
            contentContainerStyle={style.JournalEntryListEmpty}
        >
            <FontAwesomeIcon
                icon={faFeatherAlt}
                size={70}
                color={Colors.GREY_SECONDARY}
                style={style.icon}
            />

            <Text style={style.NoEntriesText}>
                An clean new journal, waiting to be filled.
            </Text>
            <Button title="Write your first entry" onPress={props.emptyListNavigationCallback} />
        </ScrollView>
    );
}

export default JournalEmptyList;