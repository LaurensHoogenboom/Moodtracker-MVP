//react
import React from 'react';
//components
import { Text, View, StyleSheet } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { ProgressBar } from 'components/atoms/progress';
//helpers
import getIconData from 'helpers/getIconData';
//style
import * as Colors from 'styles/colors';
import * as Typography from 'styles/typography';
//types
import { JournalEntry } from 'types/journalEntries';


const style = StyleSheet.create({
    content: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: Colors.BACKGROUND_LIGHT,
        width: "100%",
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 20,
        paddingTop: 20
    },
    section: {
        justifyContent: "flex-start",
        flexDirection: "column",
        maxWidth: "100%"
    },
    sectionTitle: {
        fontSize: Typography.FONT_SIZE_CAPTIONS,
        color: Colors.GREY_SECONDARY,
        marginBottom: 10
    },
    sectionContent: {
        flexDirection: "row",
        alignItems: "center"
    },
    moodTag: {
        borderRadius: 15,
        backgroundColor: Colors.BACKGROUND_NORMAL,
        flexDirection: "row",
    },
    moodTagIcon: {
        marginRight: 15
    },
    tagText: {
        color: Colors.GREY_PRIMARY,
        fontSize: Typography.FONT_SIZE_DEFAULT,
        marginRight: 15,
        lineHeight: 27
    }
})

type JournalDetailContent = {
    entry: JournalEntry
}

const JournalDetailContent = (props: JournalDetailContent) => {
    const iconData = getIconData(props.entry.mood);

    return (
        <View style={style.content}>
            <View style={style.section}>
                <Text style={style.sectionTitle}>Mood</Text>

                <View style={style.sectionContent}>
                    <View style={style.moodTag}>
                        <FontAwesomeIcon
                            icon={iconData.icon}
                            color={iconData.color}
                            size={30}
                            style={style.moodTagIcon}
                        />

                        <Text style={style.tagText}>{props.entry.mood}</Text>
                    </View>

                    <ProgressBar
                        steps={7}
                        stepsFilled={props.entry.moodIntensity}
                        foregroundColor={iconData.color}
                        containerStyle={{ marginLeft: 20 }}
                        width={100}
                    />
                </View>
            </View>
        </View>
    );
}

export default JournalDetailContent;

