//react
import React, { useContext } from 'react';
//styles
import * as Colors from 'styles/colors';
//components
import { View, Text, StyleSheet, TouchableHighlight } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
//types
import { JournalEntry } from 'types/journalEntries';
//helpers
import getDate from 'helpers/getDate';
import getIconData from 'helpers/getIconData';
//context
import JournalContext from 'context/journalContext';

const style = StyleSheet.create({
    journalEntry: {
        padding: 10,
        borderRadius: 10,
        backgroundColor: Colors.WHITE_PRIMARY,
        marginBottom: 20,
        width: "100%",
        elevation: 1
    },
    lastJournalEntry: {
        padding: 10,
        borderRadius: 10,
        backgroundColor: Colors.WHITE_PRIMARY,
        marginBottom: 0,
        width: "100%",
        elevation: 1
    },
    mainInfo: {
        flexDirection: "row",
        alignItems: "center",
        width: "100%",
    },
    journalEntryDate: {
        color: Colors.GREY_SECONDARY,
        paddingRight: 15
    },
    moodTitle: {
        paddingLeft: 15,
        color: Colors.GREY_PRIMARY
    }
})

type JournalListItem = {
    entryData: JournalEntry,
    isLastInList?: boolean
}

const JournalListItem = (props: JournalListItem) => {
    const emojiData = getIconData(props.entryData.mood);
    const journal = useContext(JournalContext);

    return (
        <TouchableHighlight
            style={props.isLastInList ? style.lastJournalEntry : style.journalEntry}
            onPress={() => {
                journal.setEntryToShowDetailFrom(props.entryData);
                journal.setShowDetailModal(true);
            }}
        >
            <View style={style.mainInfo}>
                <Text style={style.journalEntryDate}>
                    {getDate.time24(props.entryData.dateTime)}
                </Text>
                <FontAwesomeIcon
                    icon={emojiData.icon}
                    color={emojiData.color}
                    size={30}
                />
                <Text style={style.moodTitle}>
                    {props.entryData.mood}
                </Text>
            </View>
        </TouchableHighlight>
    );
}

export default JournalListItem;