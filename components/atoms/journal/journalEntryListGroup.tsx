//react
import React from 'react';
//components
import { View, Text, StyleSheet } from 'react-native';
import JournalListItem from 'components/atoms/journal/journalListItem';
//styles
import * as Colors from 'styles/colors';
import * as Typography from 'styles/typography';
//types
import { JournalDateGroup, JournalEntry } from 'types/journalEntries';
//helpers
import getDate from 'helpers/getDate';

const style = StyleSheet.create({
    NoEntries: {
        textAlign: "center"
    },
    GroupList: {
        borderLeftWidth: 2,
        borderLeftColor: Colors.GREY_SECONDARY,
        paddingLeft: 20,
        marginLeft: 5
    },
    GroupListWrapper: {
        width: "100%",
        marginBottom: 20
    },
    ListSectionDate: {
        fontSize: Typography.FONT_SIZE_DEFAULT,
        paddingBottom: 20,
        color: Colors.GREY_PRIMARY
    },
    TodayListSectionDate: {
        fontSize: Typography.FONT_SIZE_DEFAULT,
        paddingBottom: 20,
        color: Colors.BLUE_DARK
    }
})

type JournalEntryListGroup = {
    journalGroup: JournalDateGroup
    isCurrentDate: boolean
}

const JournalEntryListGroup = (props: JournalEntryListGroup) => {
    const listItems = props.journalGroup.entries.map((journalEntry, i) => {
        return (
            <JournalListItem
                entryData={journalEntry}
                key={journalEntry.id}
                isLastInList={i === props.journalGroup.entries.length - 1 ? true : false}
            />
        )
    });

    return (
        <View style={style.GroupListWrapper}>
            <Text style={props.isCurrentDate ? style.TodayListSectionDate : style.ListSectionDate}>
                {getDate.dMy(props.journalGroup.dateTime)}
            </Text>
            <View style={style.GroupList}>
                {listItems}
            </View>
        </View>
    );
}

export default JournalEntryListGroup;