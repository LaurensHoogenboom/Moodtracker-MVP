import * as Typography from 'styles/typography';
import React from 'react';
import { TextInput, StyleSheet, Text, TouchableHighlight } from 'react-native';
import * as Colors from 'styles/colors'

const styleSheet = StyleSheet.create({
    textInput: {
        borderRadius: 4,
        shadowColor: 'rgba(0,0,0,0.16)',
        backgroundColor: Colors.GREY_SECONDARY,
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 10,
        paddingRight: 10
    },
    button: {
        height: 35,
        borderRadius: 10,
        shadowColor: 'rgba(0,0,0,0.16)',
        paddingLeft: 20,
        paddingRight: 20,
        justifyContent: "center",
    },
    text: {
        color: Colors.WHITE_PRIMARY,
        width: "100%",
        textAlign: "center",
        fontSize: Typography.FONT_SIZE_DEFAULT
    }
})

type ButtonProps = {
    onPress: any,
    title: any,
    textAlign?: string,
    backgroundColor?: string,
    noBackground?: boolean,
    containerStyle?: object,
    textStyle?: object
}

const Input = () => {
    return (
        <TextInput style={styleSheet.textInput}>

        </TextInput>
    );
}

const Button = (props: ButtonProps) => {
    let backgroundColor;

    if (!props.noBackground) {
        if (props.backgroundColor) {
            backgroundColor = props.backgroundColor;
        } else {
            backgroundColor = Colors.BLUE_DARK;
        }
    }

    return (
        <TouchableHighlight
            onPress={props.onPress}
            style={[
                styleSheet.button,
                { backgroundColor: backgroundColor ? backgroundColor : 'rgba(0,0,0,0.0)' },
                props.containerStyle
            ]}
        >
            <Text
                style={[
                    styleSheet.text,
                    {
                        textAlign: props.textAlign == "left" ? "left" : "center",
                        color: backgroundColor ? Colors.WHITE_PRIMARY : Colors.GREY_PRIMARY
                    },
                    props.textStyle
                ]}
            >
                {props.title}
            </Text>
        </TouchableHighlight>
    )
}

export { Input, Button };