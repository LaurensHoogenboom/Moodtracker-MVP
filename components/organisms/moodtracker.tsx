//react
import React, { useRef } from 'react';
import { useFocusEffect } from '@react-navigation/native';
//components
import { ScrollView, StyleSheet, View, useWindowDimensions } from 'react-native';
import Moodform from 'components/molecules/moodtracker/moodForm';
import TrackingSubmitted from 'components/molecules/moodtracker/trackingSubmitted';
//dal
import { getEntries, setEntries } from 'dal/entries';
//types
import { JournalEntry } from 'types/journalEntries';

const style = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        paddingBottom: 60,
    }
})

type Moodtracker = {
    navigation: any
}

const Moodtracker = (props: Moodtracker) => {
    const formScrollViewRef = useRef<ScrollView>(null);

    const gotoFormStep = (formStep: number) => {
        if (formScrollViewRef !== null && formScrollViewRef.current !== null) {
            const scrollToY = formStep * wizardPartHeight;

            formScrollViewRef.current.scrollTo({ y: scrollToY }, undefined);
        }
    }

    const goToStart = () => {
        gotoFormStep(0);
    }

    useFocusEffect(() => {
        return (() => {
            gotoFormStep(0);
        });
    });

    const saveJournalEntry = async (entry: JournalEntry) => {
        getEntries().then((entries) => {
            if (entries) {
                entries.push(entry);
                setEntries(entries);
            } else {
                entries = [];
                entries.push(entry);
                setEntries(entries);
            }

            gotoFormStep(1);
        })
    }

    const window = useWindowDimensions();
    const wizardPartHeight = window.height - 70;

    return (
        <View style={style.container}>
            <ScrollView
                ref={formScrollViewRef}
                contentContainerStyle={{ flexGrow: 1 }}
                pagingEnabled={true}
                showsVerticalScrollIndicator={false}
                scrollEnabled={false}
            >
                <View style={{ height: wizardPartHeight }}>
                    <Moodform callback={saveJournalEntry} />
                </View>
                <View style={{ height: wizardPartHeight }}>
                    <TrackingSubmitted
                        resetForm={goToStart}
                        recommendedAction={() => props.navigation.navigate('Journal')}
                    />
                </View>
            </ScrollView>
        </View>
    );
}

export default Moodtracker;