//react
import React, { useState } from 'react';
import { useFocusEffect } from '@react-navigation/native';
//components
import { Text, View, StyleSheet } from 'react-native';
import JournalEntryList from 'components/molecules/journal/journalEntryList';
import JournalRibbon from 'components/molecules/journal/journalRibbon';
import JournalDetail from 'components/molecules/journal/journalDetail';
//dal
import { getEntries } from 'dal/entries';
//style
import * as Colors from 'styles/colors';
import * as Typography from 'styles/typography';
//types
import { JournalEntry, JournalDateGroup } from 'types/journalEntries';
//context
import JournalContext from 'context/journalContext';

const style = StyleSheet.create({
    journalContainer: {
        backgroundColor: Colors.SCREEN_BACKGROUND_DEFAULT,
        paddingTop: 20,
        paddingRight: 10,
        flex: 1,
        flexDirection: "row"
    },
    contentContainer: {
        flex: 1,
        paddingLeft: 20,
        paddingTop: 20
    },
    header: {
        paddingBottom: 20
    },
    headerText: {
        fontSize: Typography.FONT_SIZE_CAPTIONS,
        color: Colors.GREY_PRIMARY,
        fontFamily: 'AlexaStd',
    }
})

type JournalProps = {
    navigation: any
}

const sortEntriesByDate = (entries: Array<JournalEntry>) => {
    let sortedEntries = entries.sort((a: JournalEntry, b: JournalEntry) => {
        return new Date(b.dateTime).getTime() - new Date(a.dateTime).getTime();
    })

    return sortedEntries;
}

const groupEntriesByDate = (entries: Array<JournalEntry>) => {
    let groupedEntries: Array<JournalDateGroup> = [];

    entries.forEach(entry => {
        let groupExists = groupedEntries.some(group => {
            return new Date(group.dateTime).getUTCDate() == new Date(entry.dateTime).getUTCDate();
        });

        if (!groupExists) {
            groupedEntries.push({
                dateTime: entry.dateTime,
                entries: []
            });
        }
    });

    entries.forEach(entry => {
        groupedEntries.forEach(entryGroup => {
            if (new Date(entryGroup.dateTime).getUTCDate() == new Date(entry.dateTime).getUTCDate()) {
                entryGroup.entries.push(entry)
            }
        })
    });

    return groupedEntries;
}

const Journal = (props: JournalProps) => {
    const [journalEntries, setJournalEntries] = useState([]);
    const [detailVisible, setDetailVisble] = useState(false);
    const [entryToShowDetailFrom, setEntryToShowDetailFrom] = useState<JournalEntry>();

    useFocusEffect(() => {
        updateEntryList();
    });

    const updateEntryList = () => {
        getEntries().then((entries) => {
            entries = sortEntriesByDate(entries);
            entries = groupEntriesByDate(entries);

            if (JSON.stringify(journalEntries) !== JSON.stringify(entries)) {
                setJournalEntries(entries);
            }
        });
    }

    const setShowDetailModal = (isVisible: boolean) => {
        setDetailVisble(isVisible);
    };

    return (
        <JournalContext.Provider value={{ setShowDetailModal, setEntryToShowDetailFrom, updateEntryList }}>
            <View style={style.journalContainer}>
                <JournalRibbon />

                <View style={style.contentContainer}>
                    <View style={style.header}>
                        <Text style={style.headerText}> Journal</Text>
                    </View>

                    <JournalEntryList
                        journalDateGroups={journalEntries}
                        emptyListNavigationCallback={() => props.navigation.navigate('Moodtracker')}
                    />
                </View>

                <JournalDetail
                    isVisible={detailVisible}
                    entry={entryToShowDetailFrom ? entryToShowDetailFrom : journalEntries[0]}
                />
            </View>
        </JournalContext.Provider>
    );
}

export default Journal;