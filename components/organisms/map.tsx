import React from 'react';
//components
import { Text, View } from 'react-native';
import { Button } from 'components/atoms/formElements';
//dal
import { setEntries } from 'dal/entries';
//seed
import seedMoods from 'dev_seeds/moods';

const Map = () => {
    return (
        <View>
            <Text>
                Whipe
            </Text>

            <Button
                title="Remove all entries"
                onPress={() => {
                    setEntries([]).then(() => {
                        console.log("Whiped!")
                    })
                }}
            />

            <Text>
                Seed
            </Text>

            <Button
                title="Seed Data"
                onPress={seedMoods}
            />
        </View>
    );
}

export default Map;